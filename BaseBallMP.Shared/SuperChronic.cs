﻿using System;
using BaseBallMP.Shared.Interfaces;

namespace BaseBallMP.Shared
{
    public class SuperChronic<T> where T : ICloneable, IUpdatible
    {
        public readonly T Current;
        public SuperChronic(T obj)
        {
            Current = obj;
        }
        public T Simulate(TimeSpan timeDiff)
        {
            var simObj = (T) Current.Clone();
            simObj.Update(timeDiff);
            return simObj;
        }
    }
}