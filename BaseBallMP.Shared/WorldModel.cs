﻿using System;
using System.Collections.Generic;
using BaseBallMP.Shared.Interfaces;

namespace BaseBallMP.Shared
{
    public class WorldModel : ICloneable, IUpdatible
    {
        public Dictionary<int, IGameObject> GameObjects = new Dictionary<int, IGameObject>();
        private int _width;
        private int _height;
        public WorldModel(int screenWidth, int screenHeight)
        {
            _width = screenWidth;
            _height = screenHeight;
        }

        public void AddGameObject(int id, IGameObject gameObject)
        {
            GameObjects.Add(id, gameObject);
        }

        public IGameObject GetObject(int id)
        {
            return GameObjects[id];
        }

        public object Clone()
        {
            var clone = new WorldModel(_width, _height);
            foreach (var gameObject in GameObjects)
            {
                clone.GameObjects.Add(gameObject.Key, (IGameObject) gameObject.Value.Clone());
            }

            return clone;
        }

        public void RemoveGameObject(int id)
        {
            GameObjects.Remove(id);
        }

        public void Update(TimeSpan gameTime)
        {
            foreach (var gameObject in GameObjects)
            {
                gameObject.Value.Update(gameTime);
            }
        }
    }
}