﻿using System;

namespace BaseBallMP.Shared.Interfaces
{
    public interface IGameObject : IUpdatible, ICloneable
    {
        int Id { get; }

        EObjectType ObjectType { get; }
    }
}