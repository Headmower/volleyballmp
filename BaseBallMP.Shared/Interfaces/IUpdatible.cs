﻿using System;

namespace BaseBallMP.Shared.Interfaces
{
    public interface IUpdatible
    {
        void Update(TimeSpan gameTime);
    }
}
