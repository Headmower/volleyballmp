﻿using System;
using BaseBallMP.Shared.Interfaces;

namespace BaseBallMP.Shared
{
    public class PlayerModel: IGameObject
    {
        public PlayerModel(int floorY)
        {
            _floorHeight = floorY;

        }
        public int X { get; set; }
        public int Y { get; set; }
        public EPlayerInput Input { get; set; }
        public bool IsJumping { get; set; }
        public bool IsMoving { get; set; }
        public readonly int _speed = 200;
        private int _currentJumpSpeed = 0;
        private readonly int _jumpSpeed = 200;
        private readonly int _gravity = 20;
        private readonly int _floorHeight;

        public void Update(TimeSpan gameTime)
        {
            if (IsJumping)
            {
                if (Y < _floorHeight)
                {
                    Y += (int) Math.Round(-_currentJumpSpeed * gameTime.TotalSeconds);
                    _currentJumpSpeed -= (int) Math.Round(_gravity * gameTime.TotalSeconds);
                }

                if (Y >= _floorHeight)
                {
                    Y = _floorHeight;
                    _currentJumpSpeed = 0;
                    IsJumping = false;
                }

            }
            if (Input.HasFlag(EPlayerInput.Jump) && !IsJumping)
            {
                DoJump();
            }

            IsMoving = false;
            if (Input.HasFlag(EPlayerInput.Left))
            {
                IsMoving = true;
                X -= (int)Math.Round(_speed * gameTime.TotalSeconds);
            }
            if (Input.HasFlag(EPlayerInput.Right))
            {
                IsMoving = true;
                X += (int)Math.Round(_speed * gameTime.TotalSeconds);
            }
            if (Input.HasFlag(EPlayerInput.Up))
            {
                IsMoving = true;
                Y -= (int)Math.Round(_speed * gameTime.TotalSeconds);
            }
            if (Input.HasFlag(EPlayerInput.Down))
            {
                IsMoving = true;
                Y += (int)Math.Round(_speed * gameTime.TotalSeconds);
            }

        }

        private void DoJump()
        {
            _currentJumpSpeed = _jumpSpeed;
            IsJumping = true;
        }

        public object Clone()
        {
            return new PlayerModel(_floorHeight)
            {
                Id = Id,
                Input = Input,
                X = X,
                Y = Y,
                IsJumping = IsJumping,
                IsMoving = IsMoving
            };
        }

        public int Id { get; set; }
        public EObjectType ObjectType => EObjectType.Player;
    }
}