﻿namespace BaseBallMP.Shared
{
    public enum EObjectType : byte
    {
        Player,
        Ball
    }
}