﻿namespace BaseBallMP.Shared.Packets
{
    public enum EPacketType : byte
    {
        Login,
        SpawnObject,
        UserInput,
        Position
    }
}