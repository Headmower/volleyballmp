﻿using System;

namespace BaseBallMP.Shared
{
    [Flags]
    public enum EPlayerInput
    {
        None = 0,
        Jump = 1 << 0,
        Left = 1 << 1,
        Right = 1 << 2,
        Up = 1 << 3,
        Down = 1 << 4
    }
}