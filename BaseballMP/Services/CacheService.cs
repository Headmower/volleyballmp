﻿using System.Collections.Generic;

namespace BaseballMP.Services
{
    public class CacheService
    {
        private static CacheService _service;

        private Dictionary<string, object> _objects;

        private CacheService()
        {
            _objects = new Dictionary<string, object>();
        }

        public static CacheService Instance => _service ?? (_service = new CacheService());

        public void Add<T>(T obj, string key)
        {
            _objects.Add(key, obj);
        }

        public T Get<T>(string key)
        {
            return (T) _objects[key];
        }
    }
}