﻿using BaseBallMP.Shared;
using Microsoft.Xna.Framework.Input;

namespace BaseballMP.Services
{
    public static class InputService
    {
        public static EPlayerInput GetPlayerInput()
        {
            var kbState = Keyboard.GetState();
            EPlayerInput inp = EPlayerInput.None;
            if (kbState.IsKeyDown(Keys.Left))
                inp |= EPlayerInput.Left;
            if (kbState.IsKeyDown(Keys.Right))
                inp |= EPlayerInput.Right;
            if (kbState.IsKeyDown(Keys.Up))
                inp |= EPlayerInput.Up;
            if (kbState.IsKeyDown(Keys.Down))
                inp |= EPlayerInput.Down;
            if (kbState.IsKeyDown(Keys.Space))
                inp |= EPlayerInput.Jump;

            return inp;
        }
    }
}
