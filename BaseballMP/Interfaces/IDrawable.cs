﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseballMP.Entities.Interfaces
{
    public interface IDrawable
    {
        /// <summary>
        /// Draw entity. Must be called between <param name="batch">SpriteBatch</param>.Begin() and .End()
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="batch"></param>
        void Draw(GameTime gameTime, SpriteBatch batch);
    }
}