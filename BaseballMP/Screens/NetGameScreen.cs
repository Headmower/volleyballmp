﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using BaseballMP.Entities;
using BaseballMP.Entities.GameObjects;
using BaseballMP.Services;
using BaseBallMP.Shared;
using BaseBallMP.Shared.Packets;
using LiteNetLib;
using LiteNetLib.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Timer = System.Timers.Timer;

namespace BaseballMP.Screens
{
    public class NetGameScreen : ScreenBase
    {
        private World _world;
        private NetManager _client;
        private EventBasedNetListener _listener;
        private int _playerId;
        private bool _isConnected = false;
        private Timer _syncTimer = new Timer(5){AutoReset = true};
        private Thread _currentThread = Thread.CurrentThread;

        public NetGameScreen(ScreenManager manager) : base(manager)
        {
            _listener = new EventBasedNetListener();
            _client = new NetManager(_listener);
            _listener.NetworkReceiveEvent += ListenerOnNetworkReceiveEvent;
            _client.UnsyncedEvents = true;
            _client.Start();
            _syncTimer.Elapsed += SyncTimerOnElapsed;
            //_syncTimer.Start();
            _client.Connect("localhost", 1489, "VolleyballMP");

            //var player = new Player();
            //_world.AddGameObject(0, player);
        }

        private void SyncTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            _client.PollEvents();
        }

        private void ListenerOnNetworkReceiveEvent(NetPeer peer, NetPacketReader reader, DeliveryMethod deliverymethod)
        {
            var packetType = (EPacketType) reader.GetByte();
            switch (packetType)
            {
                case EPacketType.Login:
                    ProcessLoginPacket(reader);
                    break;
                case EPacketType.SpawnObject:
                    ProcessSpawnObjectPacket(reader);
                    break;
                case EPacketType.UserInput:
                    ProcessUserInputPacket(reader);
                    break;
                case EPacketType.Position:
                    ProcessPositionPacket(reader);
                    break;

            }
        }

        private void ProcessPositionPacket(NetPacketReader reader)
        {
            var id = reader.GetInt();
            if (!_world.GameObjects.ContainsKey(id))
                return;

            var x = reader.GetInt();
            var y = reader.GetInt();
            var player = (Player) _world.GameObjects[id];
            player.SetPositionFromServer(x, y);
        }

        private void ProcessUserInputPacket(NetPacketReader reader)
        {
            var id = reader.GetInt();
            if (!_world.GameObjects.ContainsKey(id))
                return;

            var input = (EPlayerInput) reader.GetInt();
            var sim = new SuperChronic<World>(_world).Simulate(-TimeSpan.FromMilliseconds(_client.FirstPeer.Ping * 2));
            
            var player = (Player) sim.GameObjects[id];
            player.Model.Input = input;
            _world = new SuperChronic<World>(_world).Simulate(TimeSpan.FromMilliseconds(_client.FirstPeer.Ping * 2));
        }

        private void ProcessSpawnObjectPacket(NetPacketReader reader)
        {
            var type = (EObjectType) reader.GetByte();
            switch (type)
            {
                case EObjectType.Player:
                    var id = reader.GetInt();
                    var x = reader.GetInt();
                    var y = reader.GetInt();
                    var pl = new Player(id, x, y);

                    _world.AddGameObject(id, pl);
                    if (_playerId.Equals(id))
                        _isConnected = true;
                    break;
            }
        }

        private void ProcessLoginPacket(NetPacketReader reader)
        {
            _playerId = reader.GetInt();
            ScreenManager.graphics.PreferredBackBufferWidth = reader.GetInt();
            ScreenManager.graphics.PreferredBackBufferHeight = reader.GetInt();
            
            _world = new World(ScreenManager.graphics.PreferredBackBufferWidth, ScreenManager.graphics.PreferredBackBufferHeight);
        }

        public override void Update(GameTime gameTime)
        {
            //if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            //{
            //    _world = new SuperChronic<World>(_world).Simulate(gameTime.ElapsedGameTime.Add(gameTime.ElapsedGameTime));
            //}
            //_client.PollEvents();

            if (!_isConnected)
                return;

            var player = (Player) _world.Model.GetObject(_playerId);
            var inp = InputService.GetPlayerInput();
            if (player.Model.Input != inp)
            {
                player.Model.Input = inp;
                SendPlayerInputPacket(player);
            }

            
            _world.Update(gameTime.ElapsedGameTime);
        }

        private void SendPlayerInputPacket(Player player)
        {
            var writer = new NetDataWriter();
            writer.Put((int)player.Model.Input);
            _client.SendToAll(writer, DeliveryMethod.Unreliable);
        }

        public override void Draw(GameTime gameTime, SpriteBatch batch)
        {
            ScreenManager.GraphicsDevice.Clear(Color.Black);

            if (!_isConnected)
                return;

            _world.Draw(gameTime, batch);
        }
    }
}