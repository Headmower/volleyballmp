﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseballMP.Screens
{
    public abstract class ScreenBase
    {
        protected ScreenManager ScreenManager { get; set; }
        public ScreenBase(ScreenManager manager)
        {
            ScreenManager = manager;
        }
        protected Dictionary<int, object> GameObjects { get; set; }
        public abstract void Update(GameTime gameTime);

        public abstract void Draw(GameTime gameTime, SpriteBatch batch);
    }
}
