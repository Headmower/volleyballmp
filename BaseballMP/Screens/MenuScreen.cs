﻿using System;
using BaseballMP.Entities.Buttons;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseballMP.Screens
{
    public class MenuScreen : ScreenBase
    {
        private StartButton _startButton;

        public MenuScreen(ScreenManager manager) : base(manager)
        {
            manager.IsMouseVisible = true;
            _startButton = new StartButton(100, 100);
            _startButton.MouseLeftClick += StartClick;
        }

        private void StartClick(object sender, EventArgs e)
        {
            StartClicked?.Invoke(this, null);
        }

        public override void Update(GameTime gameTime)
        {
            _startButton.Update(gameTime.ElapsedGameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch batch)
        {
            ScreenManager.GraphicsDevice.Clear(Color.BlanchedAlmond);

            _startButton.Draw(gameTime, batch);
        }

        public EventHandler StartClicked;
    }
}