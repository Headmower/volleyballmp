﻿using BaseballMP.Services;
using Microsoft.Xna.Framework.Graphics;

namespace BaseballMP.Entities.Buttons
{
    public class StartButton : ButtonBase
    {
        public StartButton(int x = 0, int y = 0) : base(x, y)
        {
        }

        protected override Texture2D Texture 
        {
            get => CacheService.Instance.Get<Texture2D>("Buttons/startgame_default");
            set => _ = value;
        }
    }
}