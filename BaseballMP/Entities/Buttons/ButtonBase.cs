﻿using System;
using BaseBallMP.Shared.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using IDrawable = BaseballMP.Entities.Interfaces.IDrawable;

namespace BaseballMP.Entities.Buttons
{
    public abstract class ButtonBase : IDrawable, IUpdatible
    {
        public ButtonBase(int x = 0, int y = 0)
        {
            Position = new Vector2(x, y);
        }
        public Vector2 Position { get; set; }
        protected abstract Texture2D Texture { get; set; }
        protected bool IsHover { get; set; }
        protected MouseState _prevMouseState = Mouse.GetState();

        public virtual void Draw(GameTime gameTime, SpriteBatch batch)
        {
            batch.Draw(Texture, Position, !IsHover ? Color.White : new Color(Color.Red, 200));
        }

        public virtual void Update(TimeSpan span)
        {
            var mstate = Mouse.GetState();
            if (mstate.X >= Position.X &&
                mstate.Y >= Position.Y &&
                mstate.X <= Position.X + Texture.Width &&
                mstate.Y <= Position.Y + Texture.Height)
            {
                IsHover = true;
                if (mstate.LeftButton == ButtonState.Pressed && _prevMouseState.LeftButton != ButtonState.Pressed)
                {
                    MouseLeftClick?.Invoke(this, null);
                }
            }
            else
            {
                IsHover = false;
            }

            _prevMouseState = mstate;
        }

        public EventHandler MouseLeftClick;
    }
}