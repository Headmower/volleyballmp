﻿using System;
using BaseBallMP.Shared;
using BaseBallMP.Shared.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseballMP.Entities.GameObjects
{
    public abstract class GameObjectBase : IGameObject
    {
        public abstract void Update(TimeSpan span);

        public abstract void Draw(GameTime gameTime, SpriteBatch batch);
        public abstract object Clone();

        public int Id { get; set; }
        public abstract EObjectType ObjectType { get; }
    }
}
