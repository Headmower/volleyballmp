﻿using System;
using BaseballMP.Services;
using BaseBallMP.Shared;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BaseballMP.Entities.GameObjects
{
    public class Player : GameObjectBase
    {
        public PlayerModel Model;

        public Texture2D Texture => CacheService.Instance.Get<Texture2D>("Sprites/player");

        public int _localX;
        public int _localY;
        private Vector2 _drawPosition;
        private Vector2 _nextPositionUpdateSpeed;

        public Player(int id, int x, int y)
        {
            Id = id;
            _localX = x;
            _localY = y;
            _drawPosition = new Vector2(x, y);
            _nextPositionUpdateSpeed = _drawPosition;

            Model = new PlayerModel(440)
            {
                Id = id,
                IsJumping = false,
                X = x,
                Y = y
            };
        }

        public void SetPositionFromServer(int x, int y)
        {
            Model.X = x;
            Model.Y = y;

            var nextPosUpdatePlayerModel =
                new SuperChronic<PlayerModel>(Model).Simulate(TimeSpan.FromMilliseconds(1000));
            _nextPositionUpdateSpeed = new Vector2(nextPosUpdatePlayerModel.X, nextPosUpdatePlayerModel.Y) - _drawPosition;
        }

        public override void Update(TimeSpan span)
        {
            //Model.Input = InputService.GetPlayerInput();
            Model.Update(span);

            if (!Model.IsMoving)
                return;

            _drawPosition += _nextPositionUpdateSpeed * (float) span.TotalSeconds / 1;

            //var modelPosition = new Vector2(Model.X, Model.Y);
            //var localPosition = new Vector2(_localX, _localY);
            //var diff = modelPosition - localPosition;
            //if (2.0 >= diff.Length())
            //{
            //    _drawPosition = localPosition;
            //}else
            //{
            //    _drawPosition = localPosition + diff * (float)span.TotalSeconds / 1;
            //}

            //_localX = (int)Math.Round(_drawPosition.X);
            //_localY = (int)Math.Round(_drawPosition.Y);
        }

        public override void Draw(GameTime gameTime, SpriteBatch batch)
        {
            batch.Draw(Texture, _drawPosition, Color.White);
        }

        public override object Clone()
        {
            return new Player(Id,_localX, _localY)
            {
                Model = (PlayerModel) Model.Clone()
            };
        }

        public override EObjectType ObjectType => EObjectType.Player;
    }
}
