﻿using System;
using System.Collections.Generic;
using BaseballMP.Entities.GameObjects;
using BaseballMP.Services;
using BaseBallMP.Shared;
using BaseBallMP.Shared.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using IDrawable = BaseballMP.Entities.Interfaces.IDrawable;

namespace BaseballMP.Entities
{
    public class World : IUpdatible, IDrawable, ICloneable
    {
        public WorldModel Model { get; set; }
        public Dictionary<int, GameObjectBase> GameObjects = new Dictionary<int, GameObjectBase>();
        private int _screenWidth;
        private int _screenHeight;
        private SpriteFont _font = CacheService.Instance.Get<SpriteFont>("Fonts/DefaultFont");

        public World(int screenWidth, int screenHeight)
        {
            _screenHeight = screenHeight;
            _screenWidth = screenWidth;
            Model = new WorldModel(screenWidth, screenHeight);
        }

        public void Update(TimeSpan span)
        {
            foreach (var gameObject in GameObjects)
            {
                gameObject.Value.Update(span);
            }
        }
        public void AddGameObject(int id, GameObjectBase gameObject)
        {
            GameObjects.Add(id, gameObject);
            Model.AddGameObject(id, gameObject);
        }
        public void Draw(GameTime gameTime, SpriteBatch batch)
        {
            foreach (var gameObject in GameObjects)
            {
                gameObject.Value.Draw(gameTime, batch);
            }

            var textX = 10;
            var textY = 10;
            foreach (var gameObject in GameObjects)
            {
                var player = (Player)gameObject.Value;
                var textPosition = new Vector2(textX, textY);
                batch.DrawString(_font, $"{player.Model.X:####}:{player.Model.Y:####}|{player._localX:####}:{player._localY:####}", textPosition, Color.OrangeRed);
                textY += 20;
            }
        }

        public object Clone()
        {
            var clone = new World(_screenWidth, _screenHeight);
            foreach (var gameObject in GameObjects)
            {
                clone.AddGameObject(gameObject.Key, gameObject.Value);
            }

            return clone;
        }
    }
}