﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Timers;
using BaseBallMP.Shared;
using BaseBallMP.Shared.Interfaces;
using BaseBallMP.Shared.Packets;
using LiteNetLib;
using LiteNetLib.Utils;
using Timer = System.Timers.Timer;

namespace BaseBallMP.Server
{
    public class GameHost
    {
        public NetManager Server;
        public EventBasedNetListener Listener;
        public WorldModel World;
        private Timer _updateTimer = new Timer(16.66) {AutoReset = true};
        private Timer _syncPositionTimer = new Timer(1000) {AutoReset = true};
        private bool _isActive = true;
        private int _screenWidth;
        private int _screenHeight;
        private Dictionary<NetPeer, int> _peerIds = new Dictionary<NetPeer, int>();
        private Stopwatch _stopwatch;

        public GameHost(int screenWidth, int screenHeight)
        {
            _stopwatch = new Stopwatch();
            _screenWidth = screenWidth;
            _screenHeight = screenHeight;
            Listener = new EventBasedNetListener();
            Server = new NetManager(Listener);
            Listener.ConnectionRequestEvent += ListenerOnConnectionRequestEvent;
            Listener.PeerConnectedEvent += ListenerOnPeerConnectedEvent;
            Listener.NetworkReceiveEvent += ListenerOnNetworkReceiveEvent;
            Listener.PeerDisconnectedEvent += ListenerOnPeerDisconnectedEvent;
            Server.UnsyncedEvents = true;
            World = new WorldModel(_screenWidth, _screenHeight);
            Server.Start(1489);
            _updateTimer.Elapsed += UpdateTimerOnElapsed;
            _syncPositionTimer.Elapsed += SyncPositionTimerOnElapsed;
            _updateTimer.Start();
            _stopwatch.Start();
            _syncPositionTimer.Start();
            _isActive = true;
            while (_isActive)
            {
                Thread.Sleep(16);
            }
        }

        private void ListenerOnPeerDisconnectedEvent(NetPeer peer, DisconnectInfo disconnectinfo)
        {
            var id = _peerIds[peer];
            _peerIds.Remove(peer);
            World.RemoveGameObject(id);
            Console.WriteLine($"{peer.EndPoint} disconnected: {disconnectinfo.Reason}|{disconnectinfo.SocketErrorCode}");
        }

        private void ListenerOnNetworkReceiveEvent(NetPeer peer, NetPacketReader reader, DeliveryMethod deliverymethod)
        {
            var id = _peerIds[peer];
            var input = (EPlayerInput) reader.GetInt();
            var sim = new SuperChronic<WorldModel>(World).Simulate(-TimeSpan.FromMilliseconds(peer.Ping));
            var player = (PlayerModel) sim.GetObject(id);
            //var player = (PlayerModel) World.GetObject(id);
            player.Input = input;
            World = new SuperChronic<WorldModel>(sim).Simulate(TimeSpan.FromMilliseconds(peer.Ping));
            foreach (var netPeer in Server.ConnectedPeerList)
            {
                if(netPeer.Id.Equals(peer.Id))
                    continue;

                var writer = new NetDataWriter();
                writer.Put((byte)EPacketType.UserInput);
                writer.Put(id);
                writer.Put((int)input);
                netPeer.Send(writer, DeliveryMethod.Unreliable);
            }
        }

        private void ListenerOnPeerConnectedEvent(NetPeer peer)
        {
            var player = new PlayerModel(_screenHeight)
            {
                Id = Guid.NewGuid().GetHashCode(),
                X = new Random().Next(40, 400),
                Y = new Random().Next(40, 300)
            };
            SendLoginPacket(player.Id, peer);
            _peerIds.Add(peer, player.Id);
            var sim = new SuperChronic<WorldModel>(World).Simulate(-TimeSpan.FromMilliseconds(peer.Ping));
            sim.AddGameObject(player.Id, player);
            World = new SuperChronic<WorldModel>(sim).Simulate(TimeSpan.FromMilliseconds(peer.Ping));
            SendCreateObjectPacket(World.GetObject(player.Id));
            foreach (var gameObject in World.GameObjects)
            {
                if(gameObject.Key.Equals(player.Id))
                    continue;

                SendSingleCreateObjectPacket(gameObject.Value, peer);
            }
        }

        private void SendCreateObjectPacket(IGameObject gameObject)
        {
            foreach (var netPeer in Server.ConnectedPeerList)
            {
                SendSingleCreateObjectPacket(gameObject, netPeer);
            }
        }

        private void SendSingleCreateObjectPacket(IGameObject gameObject, NetPeer peer)
        {
            var writer = new NetDataWriter();
            if (gameObject is PlayerModel player)
            {
                var sim = new SuperChronic<WorldModel>(World).Simulate(TimeSpan.FromMilliseconds(peer.Ping));
                writer.Put((byte)EPacketType.SpawnObject);
                writer.Put((byte)EObjectType.Player);
                writer.Put(player.Id);
                writer.Put(player.X);
                writer.Put(player.Y);
                peer.Send(writer, DeliveryMethod.Unreliable);
            }
        }

        private void SendInputPacket(IGameObject gameObject)
        {

        }

        private void SendLoginPacket(int id, NetPeer peer)
        {
            var writer = new NetDataWriter();
            writer.Put((byte)EPacketType.Login);
            writer.Put(id);
            writer.Put(_screenWidth);
            writer.Put(_screenHeight);
            peer.Send(writer, DeliveryMethod.ReliableOrdered);
        }

        private void SyncPositionTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            foreach (var netPeer in Server.ConnectedPeerList)
            {
                foreach (var gameObject in World.GameObjects)
                {
                    var writer = new NetDataWriter();
                    var sim = new SuperChronic<WorldModel>(World).Simulate(TimeSpan.FromMilliseconds(netPeer.Ping));
                    var simPlayer = (PlayerModel) sim.GetObject(gameObject.Key);
                    writer.Put((byte)EPacketType.Position);
                    writer.Put(simPlayer.Id);
                    writer.Put(simPlayer.X);
                    writer.Put(simPlayer.Y);
                    netPeer.Send(writer, DeliveryMethod.Unreliable);
                }
            }
        }

        private void UpdateTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            World.Update(_stopwatch.Elapsed);
            _stopwatch.Restart();
        }

        private void ListenerOnConnectionRequestEvent(ConnectionRequest request)
        {
            if (Server.PeersCount < 10)
            {
                request.AcceptIfKey("VolleyballMP");
                Console.WriteLine($"{request.RemoteEndPoint} accepted");
            }
            else
            {
                request.Reject();
                Console.WriteLine($"{request.RemoteEndPoint} rejected");
            }
        }
    }
}